<?php 
    header('Content-Type: text/html; charset=utf-8',true);
	include "../Controle/EquipeControl.php";
	include"../Controle/MusicaControl.php";
	include"../Controle/IntegranteControl.php";
	include"../Modelo/PedidoOrcamento.php";
	$id = filter_input(INPUT_GET, "id");
	$equipe = EquipeControl::getEquipeById($id);
	$dados = MusicaControl::getMusicaByEquipeId($id);
	$dados2 = IntegranteControl::getIntegrantesByEquipeId($id);
	if (!$equipe->getCodigo()) {
		header('Location: Home.php');
	}
	header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>MarriAGE - <?php echo $equipe->getNome();?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-5">
						<img style="margin-top: 20px;"src="Imagens/LogoN.jpg" class="img-rounded" alt="Logotipo do sistema MarriAGE">
					</div>
					<div class="col-md-7">
						<h1><font face="Impact"><?php echo $equipe->getNome();?></font></h1>
					</div>
				</div>	
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-4 col-md-offset-2">
				<h5><strong>Nome: </strong><small><?php echo $equipe->getNome();?></small></h5>
				<h5><strong>Email: </strong><small><?php echo $equipe->getEmail();?></small></h5>
				<?php if($equipe->getQtdCasamentos()){?>
					<h5><strong>Quantidade de casamentos efetuados: </strong><small><?php echo $equipe->getQtdCasamentos();?></small></h5>
					<h5><strong>Média de preço: </strong><small><?php echo EquipeControl::getMediaPreco($id);?></small></h5>
				<?php }else{?>
					<h5><strong>Quantidade de casamentos efetuados: </strong><small>Nenhum</small></h5>
				<?php }?>
			</div>
			<div class="col-md-6 ">
				<h5><strong>Nome do Responsável: </strong><small><?php echo $equipe->getNomeResponsavel();?></small></h5>
				<h5><strong>Email de contato: </strong><small><?php echo $equipe->getEmailResponsavel();?></small></h5>
				<h5><strong>Telefone de contato: </strong><small><?php echo $equipe->getTelefoneResponsavel();?></small></h5>
			</div>
		</div>
		<br/><br/>
		<div class="row">
			<div class="col-md-6" style="background-color: rgb(240,240.240)">
				<h3>Músicas</h3>
				<?php if(sizeof($dados)>0){?>
					<div style="overflow-y:scroll; max-height: 210px; height: 210px	">
						<table class="table table-hover	">
						    <thead>
						       	<th>Nome</th>
						        <th>Artista</th>
						        <th>Geralmente Usado Em</th>
						    </thead>
						    <tbody>
						       	<?php
						       		foreach ($dados as $dado) {
						       	?>
						       		<tr>
						       			<td><?php echo $dado->getNome();?></td>
						       			<td><?php echo $dado->getArtista();?></td>
						       			<td><?php echo $dado->getUsadoEm();?></td>
						       		</tr>
						       	<?php
						       		}
						       	?>
						       </tbody>
						</table>
					</div>
				<?php }else{?>
						<h4>Não há músicas cadastradas!	</h4>
				<?php }?>
			</div>
			<div class="col-md-6" style="background-color: rgb(240,240.240)">
				<h3>Integrantes</h3>
				<?php if(sizeof($dados2)>0){?>
					<div style="overflow-y:scroll; max-height: 210px; height: 210px">
						<table class="table table-hover">
						    <thead>
						       	<th>Nome</th>
						       	<th>Idade</th>
						        <th>Intrumento(s)</th>
						        <th>Telefone</th>
						        <th>Email</th>
						    </thead>
						    <tbody>
						       	<?php
						       		foreach ($dados2 as $dado2) {
						       	?>
						       		<tr>
						       			<td><?php echo $dado2->getNome();?></td>
						       			<td><?php echo $dado2->getIdade();?></td>
						       			<td><?php echo $dado2->getInstrumento();?></td>
						       			<td><?php echo $dado2->getFone();?></td>
						       			<td><?php echo $dado2->getEmail();?></td>
						       		</tr>
						       	<?php
						       		}
						       	?>
						       </tbody>
						</table>
					</div>
				<?php }else{?>
						<h4>Não há integrantes cadastrados!	</h4>
				<?php }?>
			</div>
		</div>
		<br/><br/>
		<div class="row">
			<div class="col-md-1 col-md-offset-5">
				<a href="Home.php" class="btn btn-danger" align="rigth">Voltar</a>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
					Orçamento
				</button>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<form action="../Controle/PedidoOrcamentoCRUD/Save.php">
				<div class="modal-dialog" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<h5 class="modal-title" id="exampleModalLabel">Orçamento - <?php echo $equipe->getNome();?></h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			      			<div class="row">
			      				<div class="col-md-4">
			      					<div class="form-group">
									    <label for="email">Nome:</label>
									    <input type="text" name="nome" class="form-control" id="nome" required="true">
									</div>
			      				</div>
			      				<div class="col-md-4">
			      					<div class="form-group">
									    <label for="email">Email:</label>
									    <input type="email" name="email" class="form-control" id="email" required="true">
									</div>
			      				</div>
			      				<div class="col-md-4">
			      					<div class="form-group">
									    <label for="email">Telefone:</label>
									    <input type="text" name="telefone" class="form-control" id="telefone" maxlength="15" required="true">
									</div>
			      				</div>
			      			</div>
			      			<div class="row">
			      				<div class="col-md-12">
			      					<div class="form-group">
									    <label for="email">Texto:</label>
			      						<textarea type="text" class="form-control" name="texto" id="texto"length="150px" required="true" rows="5" style="resize: none;"></textarea>
									</div>
			      				</div>
			      			</div>
			      			<input type="text" name="idequipe" id="idequipe" hidden="true" value="<?php echo $id;?>">
			      		</div>
			      		<div class="modal-footer">
					        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					        <button type="submit" class="btn btn-primary">Enviar pedido</button>
			      		</div>
			    	</div>
			  	</div>
		  	</form>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>