<?php
	header('Content-Type: text/html; charset=utf-8',true);
	include_once "../Controle/EquipeControl.php";
	include_once "../Modelo/Equipe.php";


	$parametro=filter_input(INPUT_GET, "parametro");
	if ($parametro) {
		$dados = EquipeControl::getAllEquipeByName($parametro);
	}else{
		$dados = EquipeControl::getAllEquipe();
	}
			
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<title>MarriAGE - Home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8" style="background-color: rgb(240,240.240)">
				<div style="padding-top: 50px;padding-bottom: 20px;" align="center">
					<img src="Imagens/LogoN.jpg" class="img-rounded" alt="Logotipo do sistema MarriAGE">
				</div>
				<h1 style="padding-bottom: 20px;" align="center">Todas as Equipes</h1>
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<form action="<?php echo $_SERVER['PHP_SELF']?>">
							<div class="input-group stylish-input-group">
			                    <input name="parametro" type="text" class="form-control"  placeholder="Pesquisar" >
			                    <span class="input-group-addon">
			                        <button type="submit" style="background: transparent; border: 0;">
			                            <span class="glyphicon glyphicon-search"></span>
			                        </button>  
			                    </span>
			                </div>
						</form>
					</div>
				</div>
				<br/><br/>
				<div align="center">
					<table class="table table-hover">
				        <thead>
				        	<th>Nome da Equipe</th>
					        <th>Email para Contato</th>
					        <th>Responsável</th>
					        <th>Ação</th>
				        </thead>
				        <tbody>
				        	<?php
				        		foreach ($dados as $dado) {
				        	?>
				        		<tr>
				        			<td><?php echo $dado->getNome();?></td>
				        			<td><?php echo $dado->getEmail();?></td>
				        			<td><?php echo $dado->getNomeResponsavel()." | ".$dado->getTelefoneResponsavel();?></td>
				        			<td><a class="btn btn-primary" href="<?php echo "VerEquipe.php?id=".$dado->getCodigo()."\"";?>">Ver</a></td>
				        		</tr>
				        	<?php
				        		}
				        	?>
				        </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>