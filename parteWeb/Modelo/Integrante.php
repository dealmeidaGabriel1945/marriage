<?php
	/**
	 * 
	 */
	class Integrante
	{

	    private $nome;
	    private $instrumento;
	    private $fone;
	    private $idade;
	    private $email;
	    private $fixo;
		
		function __construct($nome, $instrumento, $fone, $idade, $email, $fixo)
		{
			if ($nome) {$this->nome = $nome;}
			if ($instrumento) {$this->instrumento = $instrumento;}
			if ($fone) {$this->fone = $fone;}
			if ($idade) {$this->idade = $idade;}
			if ($email) {$this->email = $email;}
			if ($fixo != 0) {$this->usadoEm = "Fixo na equipe";}else{$this->usadoEm = "Não fixo";}
		}
		#GETTERS
		public function getNome()
		{
			return $this->nome;
		}
		public function getInstrumento()
		{
			return $this->instrumento;
		}
		public function getFone()
		{
			return $this->fone;
		}
		public function getIdade()
		{
			return $this->idade;
		}
		public function getEmail()
		{
			return $this->email;
		}
		public function getFixo()
		{
			return $this->fixo;
		}
		#SETTERS
		public function setNome($value)
		{
			$this->nome = $value;
		}
		public function setinstrumento($value)
		{
			$this->instrumento = $value;
		}
		public function setFone($value)
		{
			$this->fone = $value;
		}
		public function setIdade($value)
		{
			$this->idade = $value;
		}
		public function setEmail($value)
		{
			$this->email = $value;
		}
		public function setFixo($value)
		{
			if ($value)
			{
				$this->usadoEm = "Fixo na equipe";
			}else{
				$this->usadoEm = "Não fixo";
			}
		}
	}
?>