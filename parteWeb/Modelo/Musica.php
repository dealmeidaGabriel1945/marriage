<?php
	/**
	 * 
	 */
	class Musica
	{
		private $nome;
	    private $artista;
	    private $usadoEm;

		function __construct($nome, $artista, $usadoEm)
		{
			if ($nome) {$this->nome = $nome;}
			if ($artista) {$this->artista = $artista;}
			if ($usadoEm) {$this->usadoEm = $usadoEm;}

		}

		#GETTERS
		public function getNome()
		{
			return $this->nome;
		}
		public function getArtista()
		{
			return $this->artista;
		}
		public function getUsadoEm()
		{
			return $this->usadoEm;
		}
		#SETTERS
		public function setNome($value)
		{
			$this->nome = $value;
		}
		public function setArtista($value)
		{
			$this->artista = $value;
		}
		public function setUsadoEm($value)
		{
			$this->usadoEm = $value;
		}
	}
?>