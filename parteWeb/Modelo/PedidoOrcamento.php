<?php
	/**
	 * 
	 */
	class PedidoOrcamento
	{
		private $codigo;
	    private $nome;
	    private $telefone;
	    private $email;
	    private $texto;
	    private $idEquipe;
		
		function __construct($codigo, $nome,$telefone, $email, $texto, $idEquipe)
		{
			if ($codigo) {$this->codigo = $codigo;}
			if ($nome) {$this->nome = $nome;}
			if ($telefone) {$this->telefone = $telefone;}
			if ($email) {$this->email = $email;}
			if ($texto) {$this->texto = $texto;}
			if ($idEquipe) {$this->idEquipe = $idEquipe;}
		}

		#GETTERS
		public function getCodigo()
		{
			return $this->codigo;
		}
		public function getNome()
		{
			return $this->nome;
		}
		public function getEmail()
		{
			return $this->email;
		}
		public function getTelefone()
		{
			return $this->telefone;
		}
		public function getTexto()
		{
			return $this->texto;
		}
		public function getIdEquipe()
		{
			return $this->idEquipe;
		}

		#SETTERS
		public function setCodigo($value)
		{
			$this->codigo = $value;
		}
		public function setNome($value)
		{
			$this->nome = $value;
		}
		public function setEmail($value)
		{
			$this->email = $value;
		}
		public function setTelefonel($value)
		{
			$this->telefone = $value;
		}
		public function setTexto($value)
		{
			$this->texto = $value;
		}
		public function setIdEquipe($value)
		{
			$this->idEquipe = $value;
		}
	}
?>