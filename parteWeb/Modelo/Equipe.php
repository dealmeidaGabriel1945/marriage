<?php
	/**
	 * 
	 */
	class Equipe
	{
		private $codigo;
	    private $nome;
	    private $email;
	    private $nomeResponsavel;
	    private $telefoneResponsavel;
	    private $emailResponsavel;
	    private $qtdCasamentos;

		
		function __construct($codigo, $nome,$email, $nomeResponsavel, $telefoneResponsavel, $emailResponsavel, $qtdCasamentos)
		{
			if ($codigo) {$this->codigo = $codigo;}
			if ($nome) {$this->nome = $nome;}
			if ($email) {$this->email = $email;}
			if ($nomeResponsavel) {$this->nomeResponsavel = $nomeResponsavel;}
			if ($telefoneResponsavel) {$this->telefoneResponsavel = $telefoneResponsavel;}
			if ($emailResponsavel) {$this->emailResponsavel = $emailResponsavel;}
			if ($qtdCasamentos) {$this->qtdCasamentos = $qtdCasamentos;}
		}

		#GETTERS
		public function getCodigo()
		{
			return $this->codigo;
		}
		public function getNome()
		{
			return $this->nome;
		}
		public function getEmail()
		{
			return $this->email;
		}
		public function getNomeResponsavel()
		{
			return $this->nomeResponsavel;
		}
		public function getTelefoneResponsavel()
		{
			return $this->telefoneResponsavel;
		}
		public function getEmailResponsavel()
		{
			return $this->emailResponsavel;
		}
		public function getQtdCasamentos()
		{
			return $this->qtdCasamentos;
		}

		#SETTERS
		public function setCodigo($value)
		{
			$this->codigo = $value;
		}
		public function setNome($value)
		{
			$this->nome = $value;
		}
		public function setEmail($value)
		{
			$this->email = $value;
		}
		public function setNomeResponsavel($value)
		{
			$this->nomeResponsavel = $value;
		}
		public function setTelefoneResponsavel($value)
		{
			$this->telefoneResponsavel = $value;
		}
		public function setEmailResponsavel($value)
		{
			$this->emailResponsavel = $value;
		}
		public function setQtdCasamentos($value)
		{
			$this->qtdCasamentos = $value;
		}
	}
?>