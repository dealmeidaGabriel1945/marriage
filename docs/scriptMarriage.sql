-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: marriage
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `casamento`
--

DROP TABLE IF EXISTS `casamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casamento` (
  `idCasamento` int(11) NOT NULL AUTO_INCREMENT,
  `nomeContratanteCasamento` varchar(60) NOT NULL,
  `cpfContratanteCasamento` varchar(14) NOT NULL,
  `rgContratanteCasamento` varchar(45) NOT NULL,
  `telefoneContratanteCasamento` varchar(15) NOT NULL,
  `emailContratanteCasamento` text NOT NULL,
  `enderecoContratanteCasamento` text NOT NULL,
  `horaCasamento` varchar(5) NOT NULL,
  `precoTotalCasamento` float NOT NULL,
  `enderecoCasamento` text NOT NULL,
  `dataCasamento` date NOT NULL,
  `finalizadoCasamento` tinyint(1) NOT NULL,
  `confirmadoCasamento` tinyint(1) NOT NULL,
  `pagoCasamento` tinyint(1) DEFAULT NULL,
  `pagamentoCasamento` date DEFAULT NULL,
  `fk_idEquipe` int(11) NOT NULL,
  `formapagamentoCasamento` text NOT NULL,
  `noivosCasamento` text NOT NULL,
  PRIMARY KEY (`idCasamento`),
  KEY `fk_table1_equipe1_idx` (`fk_idEquipe`),
  CONSTRAINT `fk_table1_equipe1` FOREIGN KEY (`fk_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casamento`
--

LOCK TABLES `casamento` WRITE;
/*!40000 ALTER TABLE `casamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `casamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL AUTO_INCREMENT,
  `loginEquipe` varchar(45) NOT NULL,
  `senhaEquipe` varchar(45) NOT NULL,
  `nomeEquipe` varchar(45) NOT NULL,
  `caminhologoEquipe` text,
  `emailEquipe` text NOT NULL,
  `nomeResponsalvelEquipe` varchar(45) NOT NULL,
  `rgResponsavelEquipe` varchar(45) NOT NULL,
  `cpfResponsavelEquipe` varchar(14) NOT NULL,
  `emailResponsavelEquipe` text NOT NULL,
  `telefoneResponsavelEquipe` varchar(15) NOT NULL,
  `enderecoResponsavelEquipe` text NOT NULL,
  PRIMARY KEY (`idEquipe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe`
--

LOCK TABLES `equipe` WRITE;
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrante`
--

DROP TABLE IF EXISTS `integrante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrante` (
  `idIntegrante` int(11) NOT NULL AUTO_INCREMENT,
  `nomeIntegrante` varchar(45) NOT NULL,
  `cpfIntegrante` varchar(14) NOT NULL,
  `rgIntegrante` varchar(45) NOT NULL,
  `fonecontatoIntegrante` varchar(15) DEFAULT NULL,
  `emailIntegrante` text,
  `idadeIntegrante` int(11) NOT NULL,
  `enderecoIntegrante` text,
  `fotoIntegrante` text NOT NULL,
  `fk_idEquipe` int(11) NOT NULL,
  `instrumentoIntegrante` varchar(45) NOT NULL,
  `fixoIntegrante` tinyint(1) NOT NULL,
  PRIMARY KEY (`idIntegrante`),
  KEY `fk_integrante_equipe1_idx` (`fk_idEquipe`),
  CONSTRAINT `fk_integrante_equipe1` FOREIGN KEY (`fk_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integrante`
--

LOCK TABLES `integrante` WRITE;
/*!40000 ALTER TABLE `integrante` DISABLE KEYS */;
/*!40000 ALTER TABLE `integrante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrante_casamento`
--

DROP TABLE IF EXISTS `integrante_casamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrante_casamento` (
  `fk_idIntegrante` int(11) NOT NULL,
  `salarioInteCasa` float NOT NULL,
  `fk_idCasamento` int(11) NOT NULL,
  PRIMARY KEY (`fk_idIntegrante`,`fk_idCasamento`),
  KEY `fk_integrante_has_Casamento_integrante1_idx` (`fk_idIntegrante`),
  KEY `fk_integrante_casamento_table11_idx` (`fk_idCasamento`),
  CONSTRAINT `fk_integrante_casamento_table11` FOREIGN KEY (`fk_idCasamento`) REFERENCES `casamento` (`idCasamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_integrante_has_Casamento_integrante1` FOREIGN KEY (`fk_idIntegrante`) REFERENCES `integrante` (`idIntegrante`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integrante_casamento`
--

LOCK TABLES `integrante_casamento` WRITE;
/*!40000 ALTER TABLE `integrante_casamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `integrante_casamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musica`
--

DROP TABLE IF EXISTS `musica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musica` (
  `idMusica` int(11) NOT NULL AUTO_INCREMENT,
  `nomeMusica` varchar(45) NOT NULL,
  `tomMusica` varchar(3) NOT NULL,
  `artistaMusica` varchar(45) NOT NULL,
  `dificuldadeMusica` varchar(13) DEFAULT NULL,
  `tocadoemMusica` text,
  `fk_idEquipe` int(11) NOT NULL,
  `caminhoMusica` text,
  PRIMARY KEY (`idMusica`),
  KEY `fk_musica_equipe1_idx` (`fk_idEquipe`),
  CONSTRAINT `fk_musica_equipe1` FOREIGN KEY (`fk_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musica`
--

LOCK TABLES `musica` WRITE;
/*!40000 ALTER TABLE `musica` DISABLE KEYS */;
/*!40000 ALTER TABLE `musica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `musica_casamento`
--

DROP TABLE IF EXISTS `musica_casamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `musica_casamento` (
  `fk_idMusica` int(11) NOT NULL,
  `quandotocarMusiCasa` varchar(45) NOT NULL,
  `fk_idCasamento` int(11) NOT NULL,
  KEY `fk_musica_has_casamento_musica1_idx` (`fk_idMusica`),
  KEY `fk_musica_casamento_table11_idx` (`fk_idCasamento`),
  CONSTRAINT `fk_musica_casamento_table11` FOREIGN KEY (`fk_idCasamento`) REFERENCES `casamento` (`idCasamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_musica_has_casamento_musica1` FOREIGN KEY (`fk_idMusica`) REFERENCES `musica` (`idMusica`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musica_casamento`
--

LOCK TABLES `musica_casamento` WRITE;
/*!40000 ALTER TABLE `musica_casamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `musica_casamento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-26 19:48:55
