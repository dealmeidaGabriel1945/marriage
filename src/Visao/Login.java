/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import Controle.ContaControle;
import Controle.Funcoes;
import Modelo.Equipe;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.net.URL;
import javax.swing.JOptionPane;
import javax.swing.UIManager;


/**
 *
 * @author user
 */
public class Login extends javax.swing.JFrame {
    
    private ContaControle cc = new ContaControle();

    /**
     * Creates new form Login
     */
    public Login() {
        initComponents();
        //this.setExtendedState(MAXIMIZED_BOTH);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setaIcon();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PainelPrincipal = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        TFLogin = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        TFSenha = new javax.swing.JPasswordField();
        PaineSecundario = new javax.swing.JPanel();
        BTNCriarConta = new javax.swing.JButton();
        BTNSair = new javax.swing.JButton();
        BTNEntrar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("MarriAGE - Login");

        PainelPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        PainelPrincipal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PainelPrincipalKeyPressed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/Logo.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Login");

        TFLogin.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TFLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TFLoginActionPerformed(evt);
            }
        });
        TFLogin.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TFLoginKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Senha");

        TFSenha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TFSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TFSenhaKeyPressed(evt);
            }
        });

        PaineSecundario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                PaineSecundarioKeyPressed(evt);
            }
        });

        BTNCriarConta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNCriarConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/create-group-button.png"))); // NOI18N
        BTNCriarConta.setText("Criar conta");
        BTNCriarConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNCriarContaActionPerformed(evt);
            }
        });

        BTNSair.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/turn-off.png"))); // NOI18N
        BTNSair.setText("SAIR");
        BTNSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNSairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PaineSecundarioLayout = new javax.swing.GroupLayout(PaineSecundario);
        PaineSecundario.setLayout(PaineSecundarioLayout);
        PaineSecundarioLayout.setHorizontalGroup(
            PaineSecundarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaineSecundarioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PaineSecundarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BTNCriarConta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BTNSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        PaineSecundarioLayout.setVerticalGroup(
            PaineSecundarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PaineSecundarioLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BTNCriarConta, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(BTNSair, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        BTNEntrar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNEntrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/log-in.png"))); // NOI18N
        BTNEntrar.setText("ENTRAR");
        BTNEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNEntrarActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 204, 204));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Esqueci minha senha");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout PainelPrincipalLayout = new javax.swing.GroupLayout(PainelPrincipal);
        PainelPrincipal.setLayout(PainelPrincipalLayout);
        PainelPrincipalLayout.setHorizontalGroup(
            PainelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(TFLogin)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(TFSenha)
            .addComponent(BTNEntrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(PaineSecundario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PainelPrincipalLayout.setVerticalGroup(
            PainelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PainelPrincipalLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TFLogin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TFSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BTNEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(PaineSecundario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PainelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PainelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BTNCriarContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNCriarContaActionPerformed
        // TODO add your handling code here:
           CriarConta cc = new CriarConta();
           cc.setVisible(true);
           this.dispose();
    }//GEN-LAST:event_BTNCriarContaActionPerformed

    private void BTNSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNSairActionPerformed
        this.dispose();
    }//GEN-LAST:event_BTNSairActionPerformed

    private void BTNEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNEntrarActionPerformed
        // TODO add your handling code here:
        Equipe equi = new Equipe();
        equi.setLogin(TFLogin.getText());
        equi.setSenha(TFSenha.getText());
        
        int codigo = cc.logar(equi);
        
        if(codigo == -1){
            JOptionPane.showMessageDialog(this, "Login e(ou) senha inválido(s)");
        }else{
            Equipe equipeLogin = cc.buscarPorCodigo(codigo);
            Principal p = new Principal();
            p.loga(equipeLogin);
            
            p.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_BTNEntrarActionPerformed

    private void TFLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TFLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TFLoginActionPerformed

    private void PainelPrincipalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PainelPrincipalKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
            // faz qualquer coisa que você quiser  
           BTNEntrar.doClick();
           //jButton1ActionPerformed(evt); // Não funciona pois este é um ActionEvent dentro de um KeyEvent
        }  
    }//GEN-LAST:event_PainelPrincipalKeyPressed

    private void PaineSecundarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PaineSecundarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
            // faz qualquer coisa que você quiser  
           BTNEntrar.doClick();
           //jButton1ActionPerformed(evt); // Não funciona pois este é um ActionEvent dentro de um KeyEvent
        } 
    }//GEN-LAST:event_PaineSecundarioKeyPressed

    private void TFLoginKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TFLoginKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
            // faz qualquer coisa que você quiser  
           BTNEntrar.doClick();
           //jButton1ActionPerformed(evt); // Não funciona pois este é um ActionEvent dentro de um KeyEvent
        } 
    }//GEN-LAST:event_TFLoginKeyPressed

    private void TFSenhaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TFSenhaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
            // faz qualquer coisa que você quiser  
           BTNEntrar.doClick();
           //jButton1ActionPerformed(evt); // Não funciona pois este é um ActionEvent dentro de um KeyEvent
        } 
    }//GEN-LAST:event_TFSenhaKeyPressed

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        new RecuperaSenha().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLabel4MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNCriarConta;
    private javax.swing.JButton BTNEntrar;
    private javax.swing.JButton BTNSair;
    private javax.swing.JPanel PaineSecundario;
    private javax.swing.JPanel PainelPrincipal;
    private javax.swing.JTextField TFLogin;
    private javax.swing.JPasswordField TFSenha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables

    private void setaIcon() {
        URL path = getClass().getResource("/Icon/Icon.png");
        Image icon = Toolkit.getDefaultToolkit().getImage(path);
        this.setIconImage(icon);
    }
}
