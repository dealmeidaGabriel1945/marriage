/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import Controle.IntegranteControle;
import Modelo.Casamento;
import Modelo.Equipe;
import Modelo.Integrante;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author user
 */
public class InternoAdicionarCasamentoPt2 extends javax.swing.JInternalFrame {

    private Equipe equipeLogada = new Equipe();
    private Casamento casamentoObj = new Casamento();
    private IntegranteControle ic = new IntegranteControle();

    private ArrayList<Integrante> integFixo = new ArrayList<>();
    private ArrayList<Integrante> integMusicos = new ArrayList<>();
    private ArrayList<Integrante> integTerceiros = new ArrayList<>();

    private DefaultTableModel tabelaMusicosFixo;
    private DefaultTableModel tabelaMusicosTerceirizado;
    private DefaultTableModel tabelaMusicos;

    private int daMusicos = -1;
    private int daTerceirizados = -1;

    /**
     * Creates new form InternoAdicionarCasamentoPt2
     */
    public InternoAdicionarCasamentoPt2() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        TabelaMusicosFixos = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TabelaMusicosContratados = new javax.swing.JTable();
        BTNAdd = new javax.swing.JButton();
        BTNRemover = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        TabelaMusicos = new javax.swing.JTable();
        PainelAuxiliar1 = new javax.swing.JPanel();
        BTNCadastrar1 = new javax.swing.JButton();
        BTNLimparCampos = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        TFTaxa = new javax.swing.JTextField();

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Músicos Fixos:");

        TabelaMusicosFixos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        TabelaMusicosFixos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nome", "Instrumento", "Preço", "Presença"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(TabelaMusicosFixos);

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Músicos Terceirizados Escalados:");

        TabelaMusicosContratados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        TabelaMusicosContratados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nome", "Instrumento", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TabelaMusicosContratados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabelaMusicosContratadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TabelaMusicosContratados);

        BTNAdd.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/right-arrow.png"))); // NOI18N
        BTNAdd.setText("Adicionar");
        BTNAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNAddActionPerformed(evt);
            }
        });

        BTNRemover.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/left-arrow.png"))); // NOI18N
        BTNRemover.setText("Retirar");
        BTNRemover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNRemoverActionPerformed(evt);
            }
        });

        TabelaMusicos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        TabelaMusicos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nome", "Instrumento"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TabelaMusicos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabelaMusicosMouseClicked(evt);
            }
        });
        TabelaMusicos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TabelaMusicosKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(TabelaMusicos);

        PainelAuxiliar1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        BTNCadastrar1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNCadastrar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/right-arrow.png"))); // NOI18N
        BTNCadastrar1.setText("Continuar");
        BTNCadastrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNCadastrar1ActionPerformed(evt);
            }
        });

        BTNLimparCampos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BTNLimparCampos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/left-arrow.png"))); // NOI18N
        BTNLimparCampos.setText("Voltar");
        BTNLimparCampos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BTNLimparCamposActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PainelAuxiliar1Layout = new javax.swing.GroupLayout(PainelAuxiliar1);
        PainelAuxiliar1.setLayout(PainelAuxiliar1Layout);
        PainelAuxiliar1Layout.setHorizontalGroup(
            PainelAuxiliar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PainelAuxiliar1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PainelAuxiliar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BTNCadastrar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BTNLimparCampos, javax.swing.GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE))
                .addContainerGap())
        );
        PainelAuxiliar1Layout.setVerticalGroup(
            PainelAuxiliar1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PainelAuxiliar1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BTNCadastrar1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BTNLimparCampos)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Todos os Musicos Terceirizados:");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Taxa de Prestação de Serviço:");

        TFTaxa.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TFTaxa, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(PainelAuxiliar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(BTNAdd)
                                    .addComponent(BTNRemover, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BTNAdd))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BTNRemover)))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 99, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PainelAuxiliar1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(TFTaxa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void TabelaMusicosContratadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelaMusicosContratadosMouseClicked
        this.daTerceirizados = TabelaMusicosContratados.getSelectedRow();
    }//GEN-LAST:event_TabelaMusicosContratadosMouseClicked

    private void BTNAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNAddActionPerformed
        if (daMusicos < 0) {
            JOptionPane.showMessageDialog(null, "Selecione algum músico");
        } else {
            System.out.println(integMusicos.get(daMusicos).getNome());
            integTerceiros.add(integMusicos.get(daMusicos));
            integMusicos.remove(daMusicos);
            this.fazTabelaTerceiros();
            this.fazTabelaMusicos2();
            daMusicos = -1;
            this.pisca();
        }
    }//GEN-LAST:event_BTNAddActionPerformed

    private void BTNRemoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNRemoverActionPerformed
        if (daTerceirizados < 0) {
            JOptionPane.showMessageDialog(null, "Selecione algum músico");
        } else {
            integMusicos.add(integTerceiros.get(daTerceirizados));
            integTerceiros.remove(daTerceirizados);
            this.fazTabelaTerceiros();
            this.fazTabelaMusicos();
            daTerceirizados = -1;
            this.pisca();
        }
    }//GEN-LAST:event_BTNRemoverActionPerformed

    private void TabelaMusicosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelaMusicosMouseClicked
        this.daMusicos = TabelaMusicos.getSelectedRow();
    }//GEN-LAST:event_TabelaMusicosMouseClicked

    private void TabelaMusicosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TabelaMusicosKeyPressed

    }//GEN-LAST:event_TabelaMusicosKeyPressed

    private void BTNCadastrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNCadastrar1ActionPerformed

        ArrayList<Boolean> presencas = this.pegarPresencaFixos();
        boolean boo = this.conferePresenca(presencas);
        if (boo == true && integTerceiros.size() == 0) {
            JOptionPane.showMessageDialog(null, "Nenhum músico adicionado para tocar neste casamento");
        } else {
            float tot = this.conferePreco();
            if (tot < 0) {
                JOptionPane.showMessageDialog(null, "Preença o campo de preços corretamente");
            } else if (tot == 0) {
                int i = JOptionPane.showConfirmDialog(null, "Tem certeza toda a equipe tocará de graça?");
                if (i == 0) {

                    this.montaCasamento();

                    InternoAdicionarCasamentoPt3 o = new InternoAdicionarCasamentoPt3();
                    setRootPaneCheckingEnabled(false);
                    javax.swing.plaf.InternalFrameUI ifu = o.getUI();
                    ((javax.swing.plaf.basic.BasicInternalFrameUI) ifu).setNorthPane(null);
                    o.abre(equipeLogada, casamentoObj);
                    this.getParent().add(o);
                    try {
                        o.setMaximum(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    o.show();
                    this.dispose();

                } else {
                }
            } else {
                int o = JOptionPane.showConfirmDialog(null, "Deseja continuar?");
                if (o == 0) {
                    this.montaCasamento();
                    InternoAdicionarCasamentoPt3 oo = new InternoAdicionarCasamentoPt3();
                    setRootPaneCheckingEnabled(false);
                    javax.swing.plaf.InternalFrameUI ifu = oo.getUI();
                    ((javax.swing.plaf.basic.BasicInternalFrameUI) ifu).setNorthPane(null);
                    oo.abre(equipeLogada, casamentoObj);
                    this.getParent().add(oo);
                    try {
                        oo.setMaximum(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    oo.show();
                    this.dispose();
                } else {

                }
            }
        }
    }//GEN-LAST:event_BTNCadastrar1ActionPerformed

    private void BTNLimparCamposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BTNLimparCamposActionPerformed
        InternoAdicionarCasamentoPt1 o = new InternoAdicionarCasamentoPt1();
        o.abreVoltando(equipeLogada, casamentoObj);
        this.getParent().add(o);
        try {
            o.setMaximum(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        o.show();
        this.dispose();
    }//GEN-LAST:event_BTNLimparCamposActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BTNAdd;
    private javax.swing.JButton BTNCadastrar1;
    private javax.swing.JButton BTNLimparCampos;
    private javax.swing.JButton BTNRemover;
    private javax.swing.JPanel PainelAuxiliar1;
    private javax.swing.JTextField TFTaxa;
    private javax.swing.JTable TabelaMusicos;
    private javax.swing.JTable TabelaMusicosContratados;
    private javax.swing.JTable TabelaMusicosFixos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane5;
    // End of variables declaration//GEN-END:variables
    void abre(Equipe log, Casamento cas) {
        this.casamentoObj = cas;
        this.equipeLogada = log;
        this.preencheTabelas();
    }

    private void preencheTabelas() {
        this.fazTabelaFixos();
        this.fazTabelaMusicos();
        this.fazTabelaTerceiros();
    }

    //TABELA FIXOS
    private void fazTabelaFixos() {
        tabelaMusicosFixo = (DefaultTableModel) TabelaMusicosFixos.getModel();
        this.povoaTabelaFixos();

    }

    private void povoaTabelaFixos() {
        this.limparTabelaFixos();
        integFixo = ic.pegaTodosFixos(equipeLogada.getCodigo());
        for (Integrante integ : integFixo) {
            tabelaMusicosFixo.addRow(new Object[]{integ.getId(), integ.getNome(), integ.getInstrumentoPrincipal(), 0.0, Boolean.TRUE});
        }
    }

    private void limparTabelaFixos() {
        while (TabelaMusicosFixos.getRowCount() > 0) {
            DefaultTableModel dm = (DefaultTableModel) TabelaMusicosFixos.getModel();
            dm.getDataVector().removeAllElements();
        }
    }
//TABELA MUSICOS

    private void fazTabelaMusicos() {
        tabelaMusicos = (DefaultTableModel) TabelaMusicos.getModel();
        this.povoaTabelaMusicos();
    }

    private void povoaTabelaMusicos() {
        this.limparTabelaMusicos();
        this.integMusicos = ic.listarTodosIntegrantesFiltro(equipeLogada.getCodigo(), 3);
        for (Integrante integ : integMusicos) {
            tabelaMusicos.addRow(new Object[]{integ.getId(), integ.getNome(), integ.getInstrumentoPrincipal()});
        }
    }

    private void limparTabelaMusicos() {
        while (TabelaMusicos.getRowCount() > 0) {
            DefaultTableModel dm = (DefaultTableModel) TabelaMusicos.getModel();
            dm.getDataVector().removeAllElements();
        }
    }
//TABELA TERCEIROS

    private void fazTabelaTerceiros() {
        tabelaMusicosTerceirizado = (DefaultTableModel) TabelaMusicosContratados.getModel();
        this.povoaTabelaTerceirizados();
    }

    private void povoaTabelaTerceirizados() {
        this.limparTabelaTerceirizados();
        for (Integrante integ : integTerceiros) {
            tabelaMusicosTerceirizado.addRow(new Object[]{integ.getId(), integ.getNome(), integ.getInstrumentoPrincipal(), 0.0});
        }
    }

    private void limparTabelaTerceirizados() {
        while (TabelaMusicosContratados.getRowCount() > 0) {
            DefaultTableModel dm = (DefaultTableModel) TabelaMusicosContratados.getModel();
            dm.getDataVector().removeAllElements();
        }
    }

    private ArrayList<Boolean> pegarPresencaFixos() {
        ArrayList<Boolean> boos = new ArrayList<>();

        for (int i = 0; i < TabelaMusicosFixos.getRowCount(); i++) {
            boos.add((Boolean) TabelaMusicosFixos.getValueAt(i, 4));
        }
        return boos;
    }

    private boolean conferePresenca(ArrayList<Boolean> presencas) {
        ArrayList<Boolean> boos = new ArrayList<>();
        for (Boolean p : presencas) {
            if (p == false) {
                boos.add(p);
            }
        }

        if (boos.size() == presencas.size()) {
            return true;
        } else {
            return false;
        }

    }

    private void pisca() {
        TabelaMusicosContratados.setVisible(false);
        TabelaMusicos.setVisible(false);
        TabelaMusicosContratados.setVisible(true);
        TabelaMusicos.setVisible(true);
    }

    private float conferePreco() {
        float total = 0;
        try {
            for (int i = 0; i < TabelaMusicosFixos.getRowCount(); i++) {
                total += Math.abs(Float.parseFloat(TabelaMusicosFixos.getValueAt(i, 3).toString()));
            }

            for (int i = 0; i < TabelaMusicosContratados.getRowCount(); i++) {
                total += Math.abs(Float.parseFloat(TabelaMusicosContratados.getValueAt(i, 3).toString()));
            }

            return total;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private void montaCasamento() {
        ArrayList<Integrante> musicos = new ArrayList<>();
        float total = 0;

        for (int i = 0; i < TabelaMusicosFixos.getRowCount(); i++) {
            if (Boolean.parseBoolean(TabelaMusicosFixos.getValueAt(i, 4).toString()) == true) {
                integFixo.get(i).setSalario(Math.abs(Float.parseFloat(TabelaMusicosFixos.getValueAt(i, 3).toString())));
                musicos.add(integFixo.get(i));
                total += Float.parseFloat(TabelaMusicosFixos.getValueAt(i, 3).toString());
            } else {

            }
        }
        for (int i = 0; i < TabelaMusicosContratados.getRowCount(); i++) {
            integTerceiros.get(i).setSalario(Math.abs(Float.parseFloat(TabelaMusicosContratados.getValueAt(i, 3).toString())));
            musicos.add(integTerceiros.get(i));
            total += Float.parseFloat(TabelaMusicosContratados.getValueAt(i, 3).toString());
        }

        casamentoObj.setMusicos(musicos);
        try {
            total+=Float.parseFloat(TFTaxa.getText());
        } catch (Exception e) {
        }
        casamentoObj.setTotal(total);

    }

    private void fazTabelaMusicos2() {
        tabelaMusicos = (DefaultTableModel) TabelaMusicos.getModel();
        this.povoaTabelaMusicos2();
    }

    private void povoaTabelaMusicos2() {
        this.limparTabelaMusicos();
        for (Integrante integ : integMusicos) {
            tabelaMusicos.addRow(new Object[]{integ.getId(), integ.getNome(), integ.getInstrumentoPrincipal()});
        }

    }

}
