/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import Modelo.Email;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;

/**
 *
 * @author Daniel
 */
public class EnviaEmail {

    public static boolean enviar(Email e, int servidor) throws EmailException {

        try {
            MultiPartEmail email = new MultiPartEmail(); //Classe responsável por enviar o email
            int porta = getPorta(servidor);
            String portaSmtp = Integer.toString(porta); //converte a porta para String

            email.setSmtpPort(porta); //porta para envio
            email.setAuthenticator(new DefaultAuthenticator(e.getRemetente(), e.getSenha())); //autenticação da conta
            email.setDebug(true);
            String hostName = getHost(servidor);
            email.setHostName(hostName); //varia de cada servidor
            //propriedades para o envio  do email
            email.getMailSession().getProperties().put("mail.smtps.auth", true);
            email.getMailSession().getProperties().put("mail.debug", "true");
            email.getMailSession().getProperties().put("mail.smtps.port", portaSmtp);
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.port", portaSmtp);
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            email.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");
            email.getMailSession().getProperties().put("mail.smtp.starttls.enable", true);
            email.getMailSession().getProperties().put("mail.smtp.ssl.trust", hostName);
            email.setFrom(e.getRemetente(), e.getRemetente()); //email e nome de quem está enviando o email
            email.setSubject(e.getAssunto()); //Assunto do email
            email.setMsg(e.getMensagem()); //Mensagem do email
            email.addTo(e.getDestinatario()); //destinatário do email
            email.setTLS(true); //define o método de criptografia
            System.out.println(e.getCaminhoAnexo());
            if (!e.getCaminhoAnexo().equals("")) {
                System.out.println("passou aqui ..");
                EmailAttachment anexo1 = new EmailAttachment(); //Classe para anexar arquivos
                anexo1.setPath(e.getCaminhoAnexo()); //Incluindo diretório do anexo
                anexo1.setDisposition(EmailAttachment.ATTACHMENT); //Informando um email que tem anexo
                email.attach(anexo1); //Atribuindo os anexos ao email     
            } else {
            }
            email.send();
            System.out.println("Email Enviado com Sucesso !!!");
            return true;
        } catch (Exception ex) {
            System.out.println("Erro + " + ex.getMessage());
            return false;
        }

    }

    private static String getHost(int servidor) {
        String host = "";
        if (servidor == 1) {
            host = "smtp.gmail.com";
        } else if (servidor == 3) {
            host = "smtps.bol.com.br";
        } else if (servidor == 4) {
            host = "smtp.ibest.com.br";
        } else if (servidor == 5) {
            host = "smtp.ig.com.br";
        } else if (servidor == 2) {
            host = "smtp.live.com";
        }
        return host;
    }

    private static int getPorta(int servidor) {
        int porta;
        if (servidor != 2) {
            porta = 587;
        } else {
            porta = 25;
        }
        return porta;
    }
}
