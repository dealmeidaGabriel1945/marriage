/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.ConnectionFactory;
import Modelo.Integrante;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class IntegranteControle {
    
    private Connection connection;
    
    private PreparedStatement stmt;

    public IntegranteControle() {
        
        this.connection = ConnectionFactory.getConnection();
    }

    public ArrayList<Integrante> listarTodosIntegrantes(int codigo) {
        String sql =" SELECT * FROM marriage.integrante WHERE fk_idEquipe = "+codigo+" ORDER BY nomeIntegrante; ";
         
         ArrayList<Integrante> integrantes = new ArrayList<Integrante>();
         
         try {
             stmt = connection.prepareStatement(sql);            
             ResultSet rs = stmt.executeQuery();
             //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();
                
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setCpf(rs.getString(3));
                a.setRg(rs.getString(4));
                a.setFonePrincipal(rs.getString(5));
                a.setEmail(rs.getString(6));
                a.setIdade(rs.getInt(7));
                a.setEndereco(rs.getString(8));
                
                a.setFoto(rs.getString(9));
                
                a.setInstrumentoPrincipal(rs.getString(11));
                a.setFixo(rs.getBoolean(12));
                integrantes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        return integrantes;

    }

    public void cadastraIntegrante(Integrante it) {
        String sql = "INSERT INTO integrante (nomeIntegrante, cpfIntegrante, rgIntegrante, fonecontatoIntegrante, emailIntegrante, idadeIntegrante, enderecoIntegrante, "
                + "fotoIntegrante, fk_idEquipe, instrumentoIntegrante, fixoIntegrante) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
        try {
            
            stmt = connection.prepareStatement(sql);
            
            stmt.setString(1, it.getNome());
            stmt.setString(2, it.getCpf());
            stmt.setString(3, it.getRg());
            stmt.setString(4, it.getFonePrincipal());
            stmt.setString(5,it.getEmail());
            stmt.setInt(6,it.getIdade());
            stmt.setString(7,it.getEndereco());
            stmt.setString(8,it.getFoto());
            stmt.setInt(9,it.getEquipe());
            stmt.setString(10,it.getInstrumentoPrincipal());
            stmt.setBoolean(11,it.isFixo());
           
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Algo deu errado");
        }
    }

    public void exclui(Integrante integranteObj) {
        String sql = "DELETE FROM marriage.integrante WHERE idIntegrante = "+ integranteObj.getId()+";";
        try {
            stmt = connection.prepareStatement(sql);
            
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void editaIntegrante(Integrante novo, Integrante antigo, int cod) {
        String sql=" UPDATE marriage.integrante SET"
                + " nomeIntegrante = ?,"
                + " cpfIntegrante = ?,"
                + " rgIntegrante = ?,"
                + " fonecontatoIntegrante = ?,"
                + " emailIntegrante = ?,"
                + " idadeIntegrante = ?,"
                + " enderecoIntegrante = ?,"
                + " fotoIntegrante = ?,"
                + " instrumentoIntegrante = ?,"
                + " fixoIntegrante = ?"
                + " WHERE idIntegrante = ? AND fk_idEquipe = ?; ";
         try {
             stmt = connection.prepareStatement(sql);
             
                stmt.setString(1, novo.getNome());
                stmt.setString(2, novo.getCpf());
                stmt.setString(3, novo.getRg());
                stmt.setString(4, novo.getFonePrincipal());
                stmt.setString(5, novo.getEmail());
                stmt.setInt(6, novo.getIdade());
                stmt.setString(7,novo.getEndereco());
                stmt.setString(8,novo.getFoto());
                stmt.setString(9, novo.getInstrumentoPrincipal());
                stmt.setBoolean(10, novo.isFixo());
                stmt.setInt(11, antigo.getId());
                stmt.setInt(12, cod);
             
            stmt.executeUpdate();
            stmt.close();
           
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Integrante> listarTodosIntegrantesFiltro(int codigo,  int classe) {
        String sql =" SELECT * FROM marriage.integrante WHERE fk_idEquipe = "+codigo+" ";
        if(classe == 1){
            
        }else if(classe == 2){
            sql+=" AND fixoIntegrante = true ";
        }else if( classe == 3){
            sql+=" AND fixoIntegrante = false ";
        }
        
        sql+="ORDER BY nomeIntegrante ;";
         ArrayList<Integrante> integrantes = new ArrayList<Integrante>();
         
         try {
             stmt = connection.prepareStatement(sql);            
             ResultSet rs = stmt.executeQuery();
             //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();
                
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setCpf(rs.getString(3));
                a.setRg(rs.getString(4));
                a.setFonePrincipal(rs.getString(5));
                a.setEmail(rs.getString(6));
                a.setIdade(rs.getInt(7));
                a.setEndereco(rs.getString(8));
                
                a.setFoto(rs.getString(9));
                
                a.setInstrumentoPrincipal(rs.getString(11));
                a.setFixo(rs.getBoolean(12));
                integrantes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        return integrantes;

    }

    public ArrayList<Integrante> pegaTodosFixos(int codigo) {
        String sql =" SELECT * FROM marriage.integrante WHERE fk_idEquipe = "+codigo+" AND fixoIntegrante = true ORDER BY nomeIntegrante; ";
         
         ArrayList<Integrante> integrantes = new ArrayList<Integrante>();
         
         try {
             stmt = connection.prepareStatement(sql);            
             ResultSet rs = stmt.executeQuery();
             //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();
                
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setCpf(rs.getString(3));
                a.setRg(rs.getString(4));
                a.setFonePrincipal(rs.getString(5));
                a.setEmail(rs.getString(6));
                a.setIdade(rs.getInt(7));
                a.setEndereco(rs.getString(8));
                
                a.setFoto(rs.getString(9));
                
                a.setInstrumentoPrincipal(rs.getString(11));
                a.setFixo(rs.getBoolean(12));
                integrantes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        return integrantes;
    }

    public ArrayList<Integrante> pegaTodosTerceirizados(int codigo) {
        String sql =" SELECT * FROM marriage.integrante WHERE fk_idEquipe = "+codigo+" AND fixoIntegrante = false ORDER BY nomeIntegrante; ";
         
         ArrayList<Integrante> integrantes = new ArrayList<Integrante>();
         
         try {
             stmt = connection.prepareStatement(sql);            
             ResultSet rs = stmt.executeQuery();
             //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();
                
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setCpf(rs.getString(3));
                a.setRg(rs.getString(4));
                a.setFonePrincipal(rs.getString(5));
                a.setEmail(rs.getString(6));
                a.setIdade(rs.getInt(7));
                a.setEndereco(rs.getString(8));
                
                a.setFoto(rs.getString(9));
                
                a.setInstrumentoPrincipal(rs.getString(11));
                a.setFixo(rs.getBoolean(12));
                integrantes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        return integrantes;
    }

    public boolean confereRelacao(Integrante integranteObj) {
        String sql =" SELECT count(fk_idCasamento) FROM marriage.integrante_casamento Where fk_idIntegrante = "+integranteObj.getId()+";";
         
         int count = 0;
         
         try {
             stmt = connection.prepareStatement(sql);            
             ResultSet rs = stmt.executeQuery();
             //joga resultado da consulta no ArrayList
            while (rs.next()) {
                count = rs.getInt(1);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
       if(count==0){
           return false;
       }else{
           return true;
       }
    }

    
    
}
