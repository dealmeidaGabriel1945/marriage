/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.Casamento;
import Modelo.ConnectionFactory;
import Modelo.DataChart;
import Modelo.Integrante;
import Modelo.Musica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class CasamentoControle {

    private Connection connection;

    private PreparedStatement stmt;

    public CasamentoControle() {

        this.connection = ConnectionFactory.getConnection();
    }

    private int pegaUltimoCasamento(int cod) {
        String sql = " SELECT * FROM marriage.casamento WHERE fk_idEquipe = " + cod + "; ";

        int codigo = 0;

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();

                codigo = rs.getInt(1);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return codigo;
    }

    public void cadastraCasamento(Casamento cas, int cod) {
        String sql = "INSERT INTO `marriage`.`casamento` ("
                + "`nomeContratanteCasamento`,"
                + " `cpfContratanteCasamento`,"
                + " `rgContratanteCasamento`,"
                + " `telefoneContratanteCasamento`,"
                + " `emailContratanteCasamento`,"
                + " `enderecoContratanteCasamento`,"
                + " `horaCasamento`,"
                + " `precoTotalCasamento`,"
                + " `enderecoCasamento`,"
                + " `dataCasamento`,"
                + " `finalizadoCasamento`,"
                + " `confirmadoCasamento`,"
                + " `pagoCasamento`,"
                + " `pagamentoCasamento`,"
                + " `formapagamentoCasamento`,"
                + " `noivosCasamento`,"
                + " `fk_idEquipe`)"
                + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setString(1, cas.getNomeContratante());
            stmt.setString(2, cas.getCpfContratante());
            stmt.setString(3, cas.getRgContratante());
            stmt.setString(4, cas.getTelefoneContratante());
            stmt.setString(5, cas.getEmailContratante());
            stmt.setString(6, cas.getEnderecoContratante());
            stmt.setString(7, cas.getHorario());
            stmt.setFloat(8, cas.getTotal());
            stmt.setString(9, cas.getEndereco());
            stmt.setString(10, cas.getData());
            stmt.setBoolean(11, cas.isFinalizado());
            stmt.setBoolean(12, cas.isConfirmado());
            stmt.setBoolean(13, cas.isPago());
            stmt.setString(14, cas.getDataPagamento());
            stmt.setString(15, cas.getFormaPagamento());
            stmt.setString(16, cas.getNoivos());
            stmt.setInt(17, cod);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        this.cadastraMusicoCasamento(cas, cod);
        this.cadastraMusicaCasamento(cas, cod);
    }

    private void cadastraMusicoCasamento(Casamento cas, int cod) {

        int codCas = this.pegaUltimoCasamento(cod);

        for (Integrante integ : cas.getMusicos()) {

            String sql = "INSERT INTO `marriage`.`integrante_casamento` (`fk_idIntegrante`, `salarioInteCasa`, `fk_idCasamento`) VALUES (?,?,?);";
            try {
                stmt = connection.prepareStatement(sql);

                stmt.setInt(1, integ.getId());
                stmt.setFloat(2, integ.getSalario());
                stmt.setInt(3, codCas);

                stmt.execute();
                stmt.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void cadastraMusicaCasamento(Casamento cas, int cod) {
        int codCas = this.pegaUltimoCasamento(cod);

        for (Musica mus : cas.getRepertorio()) {

            String sql = "INSERT INTO `marriage`.`musica_casamento` (`fk_idMusica`, `quandotocarMusiCasa`, `fk_idCasamento`) VALUES (?,?,?);";
            try {
                stmt = connection.prepareStatement(sql);

                stmt.setInt(1, mus.getId());
                stmt.setString(2, mus.getOndeTocar());
                stmt.setInt(3, codCas);

                stmt.execute();
                stmt.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public ArrayList<Casamento> listaPorDia(int cod) {
        String sql = " SELECT * , datediff(dataCasamento,CURDATE()) AS DiasFaltando FROM marriage.casamento WHERE fk_idEquipe = " + cod + " AND finalizadoCasamento = false AND confirmadoCasamento = true  AND datediff(dataCasamento,CURDATE()) >=0 ORDER BY dataCasamento;";
        ArrayList<Casamento> casamentos = new ArrayList<>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cas.setCodigo(rs.getInt(1));
                cas.setNomeContratante(rs.getString(2));
                cas.setCpfContratante(rs.getString(3));
                cas.setRgContratante(rs.getString(4));
                cas.setTelefoneContratante(rs.getString(5));
                cas.setEmailContratante(rs.getString(6));
                cas.setEnderecoContratante(rs.getString(7));
                cas.setHorario(rs.getString(8));
                cas.setTotal(rs.getFloat(9));
                cas.setEndereco(rs.getString(10));
                cas.setData(rs.getString(11));
                cas.setFinalizado(rs.getBoolean(12));
                cas.setConfirmado(rs.getBoolean(13));
                cas.setPago(rs.getBoolean(14));
                cas.setDataPagamento(rs.getString(15));
                cas.setEquipe(rs.getInt(16));
                cas.setFormaPagamento(rs.getString(17));
                cas.setNoivos(rs.getString(18));
                cas.setDiasFaltando(rs.getInt(19));
                cas.setRepertorio(this.pegaRepertorio(cas.getCodigo(), cod));
                cas.setMusicos(this.pegaMusicos(cas.getCodigo(), cod));

                casamentos.add(cas);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return casamentos;
    }

    public ArrayList<Casamento> listaPorDiaNC(int cod) {
        String sql = " SELECT * , datediff(dataCasamento,CURDATE()) AS DiasFaltando, datediff(dataCasamento,CURDATE()) AS DiasFaltandoPagamento FROM marriage.casamento WHERE fk_idEquipe = " + cod + " AND confirmadoCasamento = false and datediff(dataCasamento,CURDATE()) >=0 ORDER BY dataCasamento;";
        ArrayList<Casamento> casamentos = new ArrayList<>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cas.setCodigo(rs.getInt(1));
                cas.setNomeContratante(rs.getString(2));
                cas.setCpfContratante(rs.getString(3));
                cas.setRgContratante(rs.getString(4));
                cas.setTelefoneContratante(rs.getString(5));
                cas.setEmailContratante(rs.getString(6));
                cas.setEnderecoContratante(rs.getString(7));
                cas.setHorario(rs.getString(8));
                cas.setTotal(rs.getFloat(9));
                cas.setEndereco(rs.getString(10));
                cas.setData(rs.getString(11));
                cas.setFinalizado(rs.getBoolean(12));
                cas.setConfirmado(rs.getBoolean(13));
                cas.setPago(rs.getBoolean(14));
                cas.setDataPagamento(rs.getString(15));
                cas.setEquipe(rs.getInt(16));
                cas.setFormaPagamento(rs.getString(17));
                cas.setNoivos(rs.getString(18));
                cas.setDiasFaltando(rs.getInt(19));
                cas.setRepertorio(this.pegaRepertorio(cas.getCodigo(), cod));
                cas.setMusicos(this.pegaMusicos(cas.getCodigo(), cod));

                casamentos.add(cas);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return casamentos;
    }

    public ArrayList<Casamento> listaPorDataPagamento(int cod) {
        String sql = "SELECT * , datediff(pagamentoCasamento,CURDATE()) AS DiasFaltando FROM marriage.casamento WHERE fk_idEquipe = "+cod+" AND confirmadoCasamento = true AND pagoCasamento = false AND finalizadoCasamento = false ORDER BY dataCasamento;";
        ArrayList<Casamento> casamentos = new ArrayList<>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cas.setCodigo(rs.getInt(1));
                cas.setNomeContratante(rs.getString(2));
                cas.setCpfContratante(rs.getString(3));
                cas.setRgContratante(rs.getString(4));
                cas.setTelefoneContratante(rs.getString(5));
                cas.setEmailContratante(rs.getString(6));
                cas.setEnderecoContratante(rs.getString(7));
                cas.setHorario(rs.getString(8));
                cas.setTotal(rs.getFloat(9));
                cas.setEndereco(rs.getString(10));
                cas.setData(rs.getString(11));
                cas.setFinalizado(rs.getBoolean(12));
                cas.setConfirmado(rs.getBoolean(13));
                cas.setPago(rs.getBoolean(14));
                cas.setDataPagamento(rs.getString(15));
                cas.setEquipe(rs.getInt(16));
                cas.setFormaPagamento(rs.getString(17));
                cas.setNoivos(rs.getString(18));
                cas.setDiasFaltando(rs.getInt(19));

                cas.setRepertorio(this.pegaRepertorio(cas.getCodigo(), cod));
                cas.setMusicos(this.pegaMusicos(cas.getCodigo(), cod));

                casamentos.add(cas);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return casamentos;
    }

    private ArrayList<Musica> pegaRepertorio(int codigoCas, int codigoEqu) {
        String sql = "SELECT * FROM marriage.musica_casamento WHERE fk_idCasamento = "+codigoCas+";";
        ArrayList<Musica> musicas = new ArrayList<Musica>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Musica a = new Musica();
                a = new MusicaControle().pegaPorId(rs.getInt(1), codigoEqu);
                a.setOndeTocar(rs.getString(2));
                musicas.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return musicas;
    }

    private ArrayList<Integrante> pegaMusicos(int codigoCas, int codigoEqu) {
        String sql = "SELECT *,salarioInteCasa FROM marriage.integrante JOIN marriage.integrante_casamento WHERE idIntegrante = fk_idIntegrante AND fk_idCasamento = " + codigoCas + " AND marriage.integrante.fk_idEquipe = " + codigoEqu + ";";

        ArrayList<Integrante> integrantes = new ArrayList<Integrante>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Integrante a = new Integrante();

                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setCpf(rs.getString(3));
                a.setRg(rs.getString(4));
                a.setFonePrincipal(rs.getString(5));
                a.setEmail(rs.getString(6));
                a.setIdade(rs.getInt(7));
                a.setEndereco(rs.getString(8));

                a.setFoto(rs.getString(9));

                a.setInstrumentoPrincipal(rs.getString(11));
                a.setFixo(rs.getBoolean(12));
                a.setSalario(rs.getFloat(16));
                integrantes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return integrantes;
    }

    public void excluiCasamento(int codigoCas, int codigoEqu) {

        this.deletaRelacaoMusica(codigoCas);
        this.deletaRelacaoIntegrante(codigoCas);

        String sql = "DELETE FROM `marriage`.`casamento` WHERE `idCasamento`='" + codigoCas + "';";

        ArrayList<Integrante> integrantes = new ArrayList<Integrante>();

        try {
            stmt = connection.prepareStatement(sql);
            stmt.execute();
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deletaRelacaoMusica(int codigoCas) {
        String sql = "DELETE FROM `marriage`.`musica_casamento` WHERE `fk_idCasamento`= '" + codigoCas + "';";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.execute();
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void deletaRelacaoIntegrante(int codigoCas) {
        String sql = "DELETE FROM `marriage`.`integrante_casamento` WHERE `fk_idCasamento`='" + codigoCas + "';";

        try {
            stmt = connection.prepareStatement(sql);
            stmt.execute();
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Casamento> listaPassados(int cod) {
        String sql = " SELECT * , datediff(dataCasamento,CURDATE()) AS DiasFaltando, datediff(dataCasamento,CURDATE()) AS DiasFaltandoPagamento FROM marriage.casamento WHERE fk_idEquipe = " + cod + " AND finalizadoCasamento = true ORDER BY dataCasamento;";
        ArrayList<Casamento> casamentos = new ArrayList<>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cas.setCodigo(rs.getInt(1));
                cas.setNomeContratante(rs.getString(2));
                cas.setCpfContratante(rs.getString(3));
                cas.setRgContratante(rs.getString(4));
                cas.setTelefoneContratante(rs.getString(5));
                cas.setEmailContratante(rs.getString(6));
                cas.setEnderecoContratante(rs.getString(7));
                cas.setHorario(rs.getString(8));
                cas.setTotal(rs.getFloat(9));
                cas.setEndereco(rs.getString(10));
                cas.setData(rs.getString(11));
                cas.setFinalizado(rs.getBoolean(12));
                cas.setConfirmado(rs.getBoolean(13));
                cas.setPago(rs.getBoolean(14));
                cas.setDataPagamento(rs.getString(15));
                cas.setEquipe(rs.getInt(16));
                cas.setFormaPagamento(rs.getString(17));
                cas.setNoivos(rs.getString(18));
                cas.setDiasFaltando(rs.getInt(19));
                cas.setRepertorio(this.pegaRepertorio(cas.getCodigo(), cod));
                cas.setMusicos(this.pegaMusicos(cas.getCodigo(), cod));

                casamentos.add(cas);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return casamentos;
    }

    public void confirmaCasamento(int codigo) {
        //UPDATE `marriage`.`casamento` SET `confirmadoCasamento`='1' WHERE `idCasamento`='15';
        String sql = "UPDATE `marriage`.`casamento` SET `confirmadoCasamento`='1' WHERE `idCasamento`= ?";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setInt(1, codigo);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void confirmaPagamentoCasamento(int codigo) {
        String sql = "UPDATE `marriage`.`casamento` SET `pagoCasamento`='1' WHERE `idCasamento`= ?";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setInt(1, codigo);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void finalizaCasamento(int codigo) {
        String sql = "UPDATE `marriage`.`casamento` SET `finalizadoCasamento`='1' WHERE `idCasamento`= ?";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setInt(1, codigo);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void editaCasamento(Casamento casamentoObj, int codigo) {
        this.deletaRelacaoIntegrante(casamentoObj.getCodigo());
        this.deletaRelacaoMusica(casamentoObj.getCodigo());

        String sql = "UPDATE `marriage`.`casamento` SET "
                + "`nomeContratanteCasamento`=?,"
                + " `cpfContratanteCasamento`=?,"
                + " `rgContratanteCasamento`=?,"//
                + " `telefoneContratanteCasamento`=?,"
                + " `emailContratanteCasamento`=?,"
                + " `enderecoContratanteCasamento`=?,"//
                + " `horaCasamento`=?,"
                + " `precoTotalCasamento`=?,"
                + " `enderecoCasamento`=?,"//
                + " `dataCasamento`=?,"
                + " `confirmadoCasamento`=?,"//
                + " `pagoCasamento`=?,"
                + " `pagamentoCasamento`=?,"
                + " `fk_idEquipe`='" + codigo + "',"
                + " `formapagamentoCasamento`=?,"
                + " `noivosCasamento`=?"
                + " WHERE `idCasamento`='" + casamentoObj.getCodigo() + "';";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setString(1, casamentoObj.getNomeContratante());
            stmt.setString(2, casamentoObj.getCpfContratante());
            stmt.setString(3, casamentoObj.getRgContratante());
            stmt.setString(4, casamentoObj.getTelefoneContratante());
            stmt.setString(5, casamentoObj.getEmailContratante());
            stmt.setString(6, casamentoObj.getEnderecoContratante());
            stmt.setString(7, casamentoObj.getHorario());
            stmt.setFloat(8, casamentoObj.getTotal());
            stmt.setString(9, casamentoObj.getEndereco());
            stmt.setString(10, casamentoObj.getData());
            stmt.setBoolean(11, casamentoObj.isConfirmado());
            stmt.setBoolean(12, casamentoObj.isPago());
            stmt.setString(13, casamentoObj.getDataPagamento());
            stmt.setString(14, casamentoObj.getFormaPagamento());
            stmt.setString(15, casamentoObj.getNoivos());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        this.cadastraMusicoCasamento(casamentoObj, codigo);
        this.cadastraMusicaCasamento(casamentoObj, codigo);
    }

    public boolean verificaMusicosEMusicas(int codigo) {

        if ((this.contaIntegrantes(codigo) <= 0) || (this.contaMusicas(codigo) <= 0)) {
            return true;
        } else {
            return false;
        }
    }

    private int contaIntegrantes(int codigo) {
        String sql = "select count(idIntegrante) FROM  marriage.integrante WHERE fk_idEquipe = " + codigo + ";";
        int cont = 0;

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cont = rs.getInt(1);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return cont;
    }

    private int contaMusicas(int codigo) {
        String sql = "select count(idMusica) FROM  marriage.musica WHERE fk_idEquipe = " + codigo + ";";
        int cont = 0;

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Casamento cas = new Casamento();

                cont = rs.getInt(1);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return cont;
    }

    public Iterable<DataChart> listaDataChart(int codigo, int ano) {
        String sql = "SELECT count(idCasamento) as 'QTD', month(dataCasamento) as 'MES' FROM marriage.casamento WHERE fk_idEquipe = '"+codigo+" ' AND year(dataCasamento) = '"+ano+"' GROUP BY month(dataCasamento);";
        
                ArrayList<DataChart> dcs = new ArrayList<>();
        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                DataChart dc = new DataChart();

                dc.setMes(Funcoes.numPraMes(rs.getInt("MES")));
                dc.setQtd(rs.getInt("QTD"));
                dcs.add(dc);
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return dcs;
    }

}
