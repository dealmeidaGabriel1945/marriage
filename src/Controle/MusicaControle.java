/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.ConnectionFactory;
import Modelo.Equipe;
import Modelo.Musica;
import java.awt.Desktop;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class MusicaControle {

    private Connection connection;

    private PreparedStatement stmt;

    public MusicaControle() {

        this.connection = ConnectionFactory.getConnection();
    }

    public void cadastraMusica(int codigo, Musica musica) {
        String sql = "INSERT INTO musica (nomeMusica, tomMusica, artistaMusica, dificuldadeMusica, tocadoemMusica, caminhoMusica, fk_idEquipe) "
                + "VALUES (?,?,?,?,?,?,?);";
        try {

            stmt = connection.prepareStatement(sql);

            stmt.setString(1, musica.getNome());
            stmt.setString(2, musica.getTom());
            stmt.setString(3, musica.getArtista());
            stmt.setString(4, musica.getDificuldade());
            stmt.setString(5, musica.getUsadoEm());
            stmt.setString(6, musica.getCaminho());
            stmt.setInt(7, codigo);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public ArrayList<Musica> listarTodosMusica(int idEquipe) {
        String sql = " SELECT * FROM marriage.musica WHERE fk_idEquipe = " + idEquipe + " ORDER BY nomeMusica; ";

        ArrayList<Musica> musicas = new ArrayList<Musica>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Musica a = new Musica();

                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setTom(rs.getString(3));
                a.setArtista(rs.getString(4));
                a.setDificuldade(rs.getString(5));
                a.setUsadoEm(rs.getString(6));
                a.setEquipe(rs.getInt(7));
                a.setCaminho(rs.getString(8));
                musicas.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return musicas;
    }

    public void abreArquivo(String url) {
        if (Desktop.isDesktopSupported()) {
            try {
                File arquivo = new File(url);
                Desktop.getDesktop().open(arquivo);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }

    public void excluiMusica(int id) {
        String sql = "DELETE FROM marriage.musica WHERE idMusica = " + id + ";";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void editaMusica(Musica musica) {
        String sql = " UPDATE marriage.musica SET"
                + " nomeMusica = ?,"
                + " tomMusica = ?,"
                + " artistaMusica = ?,"
                + " dificuldadeMusica = ?,"
                + " tocadoemMusica = ?,"
                + " caminhoMusica = ? "
                + " WHERE idMusica = ?; ";
        try {
            stmt = connection.prepareStatement(sql);

            stmt.setString(1, musica.getNome());
            stmt.setString(2, musica.getTom());
            stmt.setString(3, musica.getArtista());
            stmt.setString(4, musica.getDificuldade());
            stmt.setString(5, musica.getUsadoEm());
            stmt.setString(6, musica.getCaminho());
            stmt.setInt(7, musica.getId());

            stmt.executeUpdate();
            stmt.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<Musica> filtro(String artista, String nome, String tom, String usadoem, String dificuldade, int idEquipe) {
        String sql = " SELECT * FROM marriage.musica WHERE fk_idEquipe = " + idEquipe + " ORDER BY nomeMusica ";

        if (nome.equals("")) {
        } else {
            sql += " AND nomeMusica LIKE '" + nome + "%'";
        }
        if (artista.equals("")) {
        } else {
            sql += " AND artistaMusica LIKE '" + artista + "%'";
        }
        if (tom.equals("")) {
        } else {
            sql += " AND tomMusica LIKE '" + tom + "%'";
        }
        if (usadoem.equals("")) {
        } else {
            sql += " AND tocadoemMusica LIKE '" + usadoem + "%'";
        }
        if (dificuldade.equals("--")) {
        } else {
            sql += " AND dificuldadeMusica LIKE '" + dificuldade + "%'";
        }

        sql += ";";

        ArrayList<Musica> musicas = new ArrayList<Musica>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Musica a = new Musica();

                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setTom(rs.getString(3));
                a.setArtista(rs.getString(4));
                a.setDificuldade(rs.getString(5));
                a.setUsadoEm(rs.getString(6));
                a.setEquipe(rs.getInt(7));
                a.setCaminho(rs.getString(8));
                musicas.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return musicas;
    }

    public boolean confereRelacao(Musica musicaClick) {
        String sql = " SELECT count(fk_idCasamento) FROM marriage.musica_casamento Where fk_idMusica =  " + musicaClick.getId() + ";";

        int count = 0;

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                count = rs.getInt(1);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if (count == 0) {
            return false;
        } else {
            return true;
        }
    }

    Musica pegaPorId(int idMusica, int idEquipe) {
        String sql = " SELECT * FROM marriage.musica WHERE fk_idEquipe = " + idEquipe + " AND idMusica = " + idMusica + "; ";
        Musica a = new Musica();
        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList

            while (rs.next()) {
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setTom(rs.getString(3));
                a.setArtista(rs.getString(4));
                a.setDificuldade(rs.getString(5));
                a.setUsadoEm(rs.getString(6));
                a.setEquipe(rs.getInt(7));
                a.setCaminho(rs.getString(8));
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return a;
    }

}
