/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Guimarães
 */
public class Funcoes {

    public static String returnaHoraDoSpinner(String spinnValue) {
        char h1 = spinnValue.charAt(11);
        char h2 = spinnValue.charAt(12);
        char m1 = spinnValue.charAt(14);
        char m2 = spinnValue.charAt(15);
        char s1 = spinnValue.charAt(17);
        char s2 = spinnValue.charAt(18);
        return "" + h1 + h2 + ":" + m1 + m2 + ":" + s1 + s2;
    }

    public static Date stringParaDate(String data) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = formato.parse(data);
        } catch (ParseException ex) {
        }
        return date;
    }
    
    public static String stringDateBDParaStringNormal(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
        Date d;
        Date d2 = null;
        String data2 = "";
        try {
            d = sdf.parse(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");
            data2 = sdf2.format(d);
        } catch (ParseException ex) {
        }

        return data2;
    }

    public static String DataPorExtenso(String date) {
        String data ="";
        data += date.charAt(8)+""+date.charAt(9)+" de ";
        
        if((date.charAt(5)+""+date.charAt(6)).equals("01")){
            data+="JANEIRO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("02")){
            data+="FEVEREIRO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("03")){
            data+="MARÇO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("04")){
            data+="ABRIL ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("05")){
            data+="MAIO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("06")){
            data+="JUNHO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("07")){
            data+="JULHO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("08")){
            data+="AGOSTO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("09")){
            data+="SETEMBRO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("10")){
            data+="OUTUBRO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("11")){
            data+="NOVEMBRO ";
        }else if((date.charAt(5)+""+date.charAt(6)).equals("12")){
            data+="DEZEMRO ";
        }
        
        data+="de "+date.charAt(0)+""+date.charAt(1)+""+date.charAt(2)+""+date.charAt(3)+"";
        
        return data;
    }
    //yyyy-MM-dd

    public static String numPraMes(int num) {
        if(num==1){
            return "Jan";
        }else if(num==2){
            return "Fev";
        }else if(num==3){
            return "Mar";
        }else if(num==4){
            return "Abr";
        }else if(num==5){
            return "Mai";
        }else if(num==6){
            return "Jun";
        }else if(num==7){
            return "Jul";
        }else if(num==8){
            return "Ago";
        }else if(num==9){
            return "Set";
        }else if(num==10){
            return "Out";
        }else if(num==11){
            return "Nov";
        }else if(num==12){
            return "Dez";
        }else{
            return "OLOCO MEU";
        }
    }
}
