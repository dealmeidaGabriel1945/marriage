/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.ConnectionFactory;
import Modelo.Equipe;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class ContaControle {

    private Connection connection;

    private PreparedStatement stmt;

    public ContaControle() {

        this.connection = ConnectionFactory.getConnection();
    }

    public ArrayList<Equipe> listarTodosEquipe() {
        String sql = " SELECT * FROM marriage.equipe; ";

        ArrayList<Equipe> equipes = new ArrayList<Equipe>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Equipe a = new Equipe();

                a.setCodigo(rs.getInt(1));
                a.setLogin(rs.getString(2));
                a.setSenha(rs.getString(3));
                a.setNome(rs.getString(4));
                a.setEmail(rs.getString(6));
                equipes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return equipes;
    }

    public boolean confereCadastro(ArrayList<Equipe> equipes, Equipe equi) {
        boolean preocupacao = false;
        for (Equipe e : equipes) {
            if ((e.getLogin().equals(equi.getLogin())) || (e.getNome().equals(equi.getNome())) || (e.getSenha().equals(equi.getSenha()))) {
                preocupacao = true;
            }
        }
        return preocupacao;
    }

    public void cadstraEquipe(Equipe eq) {
        //INSERT INTO `marriage`.`equipe` (`loginEquipe`, `senhaEquipe`, `nomeEquipe`, `emailEquipe`, `nomeResponsalvelEquipe`, `rgResponsavelEquipe`, `cpfResponsavelEquipe`, `emailResponsavelEquipe`, `telefoneResponsavelEquipe`, `enderecoResponsavelEquipe`) VALUES ('aa', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a');

        String sql = "insert into marriage.equipe (loginEquipe,senhaEquipe,nomeEquipe,emailEquipe, nomeResponsalvelEquipe, rgResponsavelEquipe, cpfResponsavelEquipe, emailResponsavelEquipe, telefoneResponsavelEquipe, enderecoResponsavelEquipe,pergunta1Equipe,"
                + "resposta1Equipe,pergunta2Equipe,resposta2Equipe,pergunta3Equipe,resposta3Equipe)"
                + "values "
                + "(?, md5(?), ?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        try {
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, eq.getLogin());
            stmt.setString(2, eq.getSenha());
            stmt.setString(3, eq.getNome());
            stmt.setString(4, eq.getEmail());
            stmt.setString(5, eq.getNomeResponsavel());
            stmt.setString(6, eq.getRgResponsavel());
            stmt.setString(7, eq.getCpfResponsavel());
            stmt.setString(8, eq.getEmailResponsavel());
            stmt.setString(9, eq.getTelefoneResponsavel());
            stmt.setString(10, eq.getEnderecoResponsavel());
            stmt.setString(11, eq.getPergunta1());
            stmt.setString(12, eq.getResposta1());
            stmt.setString(13, eq.getPergunta2());
            stmt.setString(14, eq.getResposta2());
            stmt.setString(15, eq.getPergunta3());
            stmt.setString(16, eq.getResposta3());
            stmt.execute();
            stmt.close();
            this.mostraPDF(eq);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int logar(Equipe equi) {
        /*ArrayList<Equipe> equipes = this.listarTodosEquipe();
        int codigo = -1;
        
        for (Equipe equipe : equipes) {
            if((equipe.getLogin().equals(equi.getLogin())) && (equipe.getSenha().equals(equi.getSenha()))){
                codigo = equipe.getCodigo();
            }
        }
        
        return codigo;*/

        String sql = " SELECT * FROM marriage.equipe WHERE loginEquipe = '" + equi.getLogin() + "' AND senhaEquipe = md5('" + equi.getSenha() + "');";

        ArrayList<Equipe> equipes = new ArrayList<Equipe>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Equipe a = new Equipe();

                a.setCodigo(rs.getInt(1));
                a.setLogin(rs.getString(2));
                a.setSenha(rs.getString(3));
                a.setNome(rs.getString(4));
                equipes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if ((equipes.size() > 1) || (equipes.size() == 0)) {
            return -1;
        } else {
            return equipes.get(0).getCodigo();
        }

    }

    public Equipe buscarPorCodigo(int codigo) {
        String sql = " SELECT * FROM marriage.equipe WHERE idEquipe = " + codigo + "; ";
        Equipe a = new Equipe();
        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {

                a.setCodigo(rs.getInt(1));
                a.setLogin(rs.getString(2));
                a.setSenha(rs.getString(3));
                a.setNome(rs.getString(4));
                a.setEmail(rs.getString(5));
                a.setNomeResponsavel(rs.getString(6));
                a.setRgResponsavel(rs.getString(7));
                a.setCpfResponsavel(rs.getString(8));
                a.setEmailResponsavel(rs.getString(9));
                a.setTelefoneResponsavel(rs.getString(10));
                a.setEnderecoResponsavel(rs.getString(11));
                a.setPergunta1(rs.getString(12));
                a.setResposta1(rs.getString(13));
                a.setPergunta2(rs.getString(14));
                a.setResposta2(rs.getString(15));
                a.setPergunta3(rs.getString(16));
                a.setResposta3(rs.getString(17));

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return a;
    }

    public boolean confereSenha(int codigo, String senha) {
        String sql = " SELECT idEquipe FROM marriage.equipe WHERE idEquipe = " + codigo + " AND senhaEquipe = md5('" + senha + "');";

        ArrayList<Equipe> equipes = new ArrayList<Equipe>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                Equipe a = new Equipe();

                a.setCodigo(rs.getInt(1));
                equipes.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        if ((equipes.size() > 1) || (equipes.size() == 0)) {
            return false;
        } else {
            return true;
        }
    }

    public void atualizaEquipe(Equipe eq) {
        String sql = " UPDATE marriage.equipe SET"
                + " loginEquipe = ?,"//
                + " nomeEquipe = ?,"//
                + " emailEquipe = ?,"//
                + " nomeResponsalvelEquipe = ?,"
                + " rgResponsavelEquipe = ?,"
                + " cpfResponsavelEquipe = ?,"
                + " emailResponsavelEquipe = ?,"
                + " telefoneResponsavelEquipe = ?,"
                + " enderecoResponsavelEquipe = ?"
                + " WHERE idEquipe = "+eq.getCodigo()+";";
        try {
            stmt = connection.prepareStatement(sql);
            
            
            stmt.setString(1, eq.getLogin());
            stmt.setString(2, eq.getNome());
            stmt.setString(3, eq.getEmail());
            stmt.setString(4, eq.getNomeResponsavel());
            stmt.setString(5, eq.getRgResponsavel());
            stmt.setString(6, eq.getCpfResponsavel());
            stmt.setString(7, eq.getEmailResponsavel());
            stmt.setString(8, eq.getTelefoneResponsavel());
            stmt.setString(9, eq.getEnderecoResponsavel());

            stmt.executeUpdate();
            stmt.close();
            JOptionPane.showMessageDialog(null, "Atualização feita com sucesso. Reefetue o login.");

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Algo deu errado");
        }
    }

    private void mostraPDF(Equipe eq) {
        try {
            GeraPDF.fazPDFSenha(eq);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContaControle.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(ContaControle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Equipe buscarPorNome(String nome) {
        String sql = " SELECT * FROM marriage.equipe WHERE nomeEquipe = '" + nome + "'; ";
        Equipe a = new Equipe();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {

                a.setCodigo(rs.getInt(1));
                a.setLogin(rs.getString(2));
                a.setSenha(rs.getString(3));
                a.setNome(rs.getString(4));
                a.setEmail(rs.getString(6));
                a.setNomeResponsavel(rs.getString(7));
                a.setRgResponsavel(rs.getString(8));
                a.setCpfResponsavel(rs.getString(9));
                a.setEmailResponsavel(rs.getString(10));
                a.setTelefoneResponsavel(rs.getString(11));
                a.setEnderecoResponsavel(rs.getString(12));
                a.setPergunta1(rs.getString(13));
                a.setResposta1(rs.getString(14));
                a.setPergunta2(rs.getString(15));
                a.setResposta2(rs.getString(16));
                a.setPergunta3(rs.getString(17));
                a.setResposta3(rs.getString(18));
            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return a;
    }

    public void editaSenha(int cod, String senha) {
        String sql = " UPDATE marriage.equipe SET"
                + " senhaEquipe = md5('"+senha+"')"
                + " WHERE idEquipe = "+cod+";";
        try {
            stmt = connection.prepareStatement(sql);
            stmt.executeUpdate();
            stmt.close();
            JOptionPane.showMessageDialog(null, "Atualização feita com sucesso.");

        } catch (SQLException e) {
            System.err.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Algo deu errado");
        }
    }
}
