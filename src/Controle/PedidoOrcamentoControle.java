/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import Modelo.ConnectionFactory;
import Modelo.PedidoOrcamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Guimarães
 */
public class PedidoOrcamentoControle {
     private Connection connection;

    private PreparedStatement stmt;

    public PedidoOrcamentoControle() {
        this.connection = ConnectionFactory.getConnection();
    }
    
    public ArrayList<PedidoOrcamento> listarTodosPedidos(int idEquipe) {
        String sql = " SELECT * FROM marriage.pedidoorcamento WHERE fk_idEquipe = " + idEquipe + "; ";

        ArrayList<PedidoOrcamento> musicas = new ArrayList<PedidoOrcamento>();

        try {
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            //joga resultado da consulta no ArrayList
            while (rs.next()) {
                PedidoOrcamento a = new PedidoOrcamento();

                a.setCodigo(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setEmail(rs.getString(3));
                a.setTelefone(rs.getString(4));
                a.setTexto(rs.getString(5));
                a.setIdEquipe(rs.getInt(6));
                musicas.add(a);

            }
            stmt.close();//fecha conexão - OBRIGATORIO SEMPRE!
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return musicas;
    }
    
    public boolean deletarPedido(int codigo) {
        boolean boo = false;
        String sql = "DELETE FROM `marriage`.`pedidoorcamento` WHERE `idPedOrc`='"+codigo+"';";
        try {
            stmt = connection.prepareStatement(sql);
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            boo = true;
        }

        return boo;
    }
}
