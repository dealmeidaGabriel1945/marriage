/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class Casamento {
    
    private int codigo;
    private int equipe;
    private String noivos;
    private String nomeContratante;
    private String rgContratante;
    private String cpfContratante;
    private String telefoneContratante;
    private String enderecoContratante;
    private String emailContratante;
    private String horario;
    private String data;
    private String endereco;
    private boolean finalizado;
    private String dataPagamento; 
    private boolean pago;
    private boolean confirmado;
    
    private ArrayList<Integrante> musicos = new ArrayList<>();
    private ArrayList<Musica> repertorio = new ArrayList<>();
    private float total;
    private String formaPagamento;
    
    private int diasFaltando;
    private int diasFaltandoPagamento;

    public int getDiasFaltandoPagamento() {
        return diasFaltandoPagamento;
    }

    public void setDiasFaltandoPagamento(int diasFaltandoPagamento) {
        this.diasFaltandoPagamento = diasFaltandoPagamento;
    }

    public int getDiasFaltando() {
        return diasFaltando;
    }

    public void setDiasFaltando(int diasFaltando) {
        this.diasFaltando = diasFaltando;
    }

    public String getNoivos() {
        return noivos;
    }

    public void setNoivos(String noivos) {
        this.noivos = noivos;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public ArrayList<Integrante> getMusicos() {
        return musicos;
    }

    public void setMusicos(ArrayList<Integrante> musicos) {
        this.musicos = musicos;
    }

    public ArrayList<Musica> getRepertorio() {
        return repertorio;
    }

    public void setRepertorio(ArrayList<Musica> repertorio) {
        this.repertorio = repertorio;
    }
    
    
    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getEquipe() {
        return equipe;
    }

    public void setEquipe(int equipe) {
        this.equipe = equipe;
    }

    public String getHorario() {
        return horario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    

    public boolean isConfirmado() {
        return confirmado;
    }

    public void setConfirmado(boolean confirmado) {
        this.confirmado = confirmado;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    public String getNomeContratante() {
        return nomeContratante;
    }

    public void setNomeContratante(String nomeContratante) {
        this.nomeContratante = nomeContratante;
    }

    public String getRgContratante() {
        return rgContratante;
    }

    public void setRgContratante(String rgContratante) {
        this.rgContratante = rgContratante;
    }

    public String getCpfContratante() {
        return cpfContratante;
    }

    public void setCpfContratante(String cpfContratante) {
        this.cpfContratante = cpfContratante;
    }

    public String getTelefoneContratante() {
        return telefoneContratante;
    }

    public void setTelefoneContratante(String telefoneContratante) {
        this.telefoneContratante = telefoneContratante;
    }

    public String getEnderecoContratante() {
        return enderecoContratante;
    }

    public void setEnderecoContratante(String enderecoContratante) {
        this.enderecoContratante = enderecoContratante;
    }

    public String getEmailContratante() {
        return emailContratante;
    }

    public void setEmailContratante(String emailContratante) {
        this.emailContratante = emailContratante;
    }
    
    
    
    
}
