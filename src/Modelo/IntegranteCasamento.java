/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author user
 */
public class IntegranteCasamento {
    
    private int idIntegrante;
    private float salario;
    private int idCasamento;
    
    public void IntegranteCasamento(int codInteg, int codCas, float sala){
        this.setIdCasamento(codCas);
        this.setIdIntegrante(codInteg);
        this.setSalario(sala);
    }

    public int getIdIntegrante() {
        return idIntegrante;
    }

    public void setIdIntegrante(int idIntegrante) {
        this.idIntegrante = idIntegrante;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public int getIdCasamento() {
        return idCasamento;
    }

    public void setIdCasamento(int idCasamento) {
        this.idCasamento = idCasamento;
    }
    
    
}
