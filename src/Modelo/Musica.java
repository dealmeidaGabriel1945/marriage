/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author user
 */
public class Musica {
    
    private int id;
    private String nome;
    private String artista;
    private String tom;
    private String dificuldade;
    private String usadoEm;
    private String caminho;
    private int equipe;
    
    private String ondeTocar;

    public String getOndeTocar() {
        return ondeTocar;
    }

    public void setOndeTocar(String ondeTocar) {
        this.ondeTocar = ondeTocar;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public int getEquipe() {
        return equipe;
    }

    public void setEquipe(int equipe) {
        this.equipe = equipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getTom() {
        return tom;
    }

    public void setTom(String tom) {
        this.tom = tom;
    }

    public String getDificuldade() {
        return dificuldade;
    }

    public void setDificuldade(String dificuldade) {
        this.dificuldade = dificuldade;
    }

    public String getUsadoEm() {
        return usadoEm;
    }

    public void setUsadoEm(String usadoEm) {
        this.usadoEm = usadoEm;
    }
    
    
    
    
}
